module Main where

import Day2
import Day3
import Data.List (transpose)
import System.Environment (getArgs)
import Text.Megaparsec
import Text.Megaparsec.Char.Lexer (binary)

parseFromFile p file = runParser p file <$> readFile file

main :: IO ()
main = do
    args <- getArgs
    let file = if null args then "test.txt" else head args
    (Right nums) <- parseFromFile numbers file
    let gam = gamma nums
    let eps = epsilon nums
    print $ (*) <$> binStrToDec gam <*> binStrToDec eps
