{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}

module Day2 where

import Optics
import Control.Monad (ap)
import Data.Foldable (foldl')
import Data.Functor (($>))
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import Data.Char (toUpper)
import Data.Maybe (fromMaybe)

type Parser = Parsec Void String
data Direction = Forward | Down | Up deriving Read

direction :: Parser Direction
direction = read . over (ix 0) toUpper <$> (string "forward" <|> string "down" <|> string "up")

command :: Parser (Direction, Int)
command = do
  dir <- direction
  char ' '
  x <- read <$> some digitChar
  pure (dir, x)

commands :: Parser [(Direction, Int)]
commands = some (command <* newline)

moveSub :: [(Direction, Int)] -> (Int, Int)
moveSub = foldr move (0, 0)
  where
    move :: (Direction, Int) -> (Int, Int) -> (Int, Int)
    move (s, n) (x, y) = case s of
      Forward -> (x + n, y)
      Down -> (x, y + n)
      Up -> (x, y - n)

data SubPosition = SubPosition
  { _horizontal :: Int,
    _depth :: Int,
    _aim :: Int
  }
  deriving (Show)

makeLenses ''SubPosition

moveSubAim :: [(Direction, Int)] -> SubPosition
moveSubAim = foldl' move (SubPosition 0 0 0)
  where
    move :: SubPosition -> (Direction, Int) -> SubPosition
    move pos (s, n) = case s of
      Down -> pos & aim +~ n
      Up -> pos & aim -~ n
      Forward -> pos & horizontal +~ n & depth +~ (n * (pos ^. aim))
(+~) :: (Is k A_Setter, Num b) => Optic k is s t b b -> b -> s -> t
l +~ n = over l (+ n)
{-# INLINE (+~) #-}

(-~) :: (Is k A_Setter, Num b) => Optic k is s t b b -> b -> s -> t
l -~ n = over l (subtract n)
{-# INLINE (-~) #-}
