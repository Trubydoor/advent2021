module Day3 where

import Data.Function
import Data.List
import Data.Ord
import Data.Void
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer (binary)

type Parser = Parsec Void String

number :: Parser [Char]
number = some binDigitChar

numbers :: Parser [String]
numbers = some (number <* newline)

mostCommon :: String -> Char
mostCommon = head . maximumBy (compare `on` length) . group . sort

gamma :: [String] -> String
gamma = map mostCommon . transpose

epsilon :: [String] -> String
epsilon = map leastCommon . transpose

binStrToDec :: String -> Maybe Int
binStrToDec = parseMaybe (binary :: Parser Int)

leastCommon :: String -> Char
leastCommon = head . minimumBy (compare `on` length) . group . sort