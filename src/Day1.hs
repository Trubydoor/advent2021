module Day1 where

increasingPairs :: [Int] -> Int
increasingPairs xs = length . filter id $ zipWith (<) xs (tail xs)

increasingTriples :: [Int] -> Int
increasingTriples xs = increasingPairs $ zipWith3 (\a b c -> a + b + c) xs (tail xs) (tail $ tail xs)
